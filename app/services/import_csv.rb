# frozen_string_literal: true

require 'csv'

class ImportCsv
  def self.parse_csv(file)
    csv_text = File.read(file)
    parsed_csv = CSV.parse(csv_text, headers: true)
    temp = parsed_csv.each.with_object({}) do |row, hash|
      hash[row["id"]] = {
        "amountCents": row["amountCents"],
        "timestamp": row["timestamp"],
        "tags": row["tags"],
        "vendor": row["vendor"]
      }
    end
  end
end
