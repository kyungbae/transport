# frozen_string_literal: true

class HomeController < ApplicationController
  def index
    parsed = ImportCsv.parse_csv('mobility.csv')

    @choices = process_parsed_data(parsed)
  end

  private

  def process_parsed_data(parsed_data)
    count_hash = parsed_data.each.with_object({}) do |item, hash|
      choice = item.last[:tags].split(',').last.titleize

      if hash.keys.include? choice
        hash[choice] += 1
      else
        hash[choice] = 1
      end
    end

    return count_hash
  end
end
