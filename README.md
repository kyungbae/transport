# README #

This is my submission for the Culdesac take-home project.

### Tech stack
* Ruby 3
* Ruby on Rails 7
* Chartkick for drawing a chart, [https://github.com/ankane/chartkick](https://github.com/ankane/chartkick)

### How do I get set up? ###

* Make sure you have ruby installed
* Run `bundle install` in the repository directory to install all dependencies
* Run `bundle exec rails s` to run a rails server
* Open web browser with the URL `localhost:3000`

